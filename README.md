## 前言

### 🍊缘由

#### 阿里AI一出马，gpt都得拉下把

![](https://img.javadog.net/blog/alibaba-ai/a84a4cc5a37d4b0b9c9c3e9ed18cf2c8.png)

🏀闪亮主角：

大家好，我是[JavaDog程序狗](https://mp.weixin.qq.com/s/FjOdxCAPFrJpruhS1YB-MA)

今天跟大家分享**Spring Cloud Alibaba AI**， 以Spring AI 为基，并提供阿里云通义大模型的 Java AI 应用。

本狗以**SpringBoot+uniapp+uview2对接Spring Cloud Alibaba AI**，带你玩转实现自己的**聊天小AI**。

![](https://img.javadog.net/blog/alibaba-ai/63dd7f915be142d7a8c41dec257fefb8.png)

📘你想听的故事

狗哥在翻阅之前写的文章时，发现很多[对接gpt的实操项目文章](https://javadog.net)都被和谐了，而且老美还限制我们爱国青年使用gpt

![](https://img.javadog.net/blog/alibaba-ai/67296c92bff14fd6a6a86bbed6105c7a.png)

呸，老美这卑鄙手段。所以从我做起，**拒绝黄拒绝赌拒绝gpt**，找一个平替的国内AI大模型，让gpt食屎去吧！

Spring Cloud Alibaba AI 阿里国产的通义AI大模型，绝对是平替gpt的梦中情AI，就让本狗带大家手把手一起玩耍下吧。

![](https://img.javadog.net/blog/alibaba-ai/a06fa563006b43e58a9c82f5ebbe7032.gif)

### 🎁如何获取源码

公众号：【JavaDog程序狗】

关注公众号，**发送  “alibaba-ai”**，无任何套路即可获得！

![](https://img.javadog.net/blog/alibaba-ai/a2475da81e9f46bea97b0580693b33bc.png)

## 正文

### 🎯主要目标

#### 实现3大重点

##### 1. 了解Spring Cloud Alibaba AI 是什么

##### 2. 使用Spring Cloud Alibaba AI 前置条件

##### 3. SpringBoot+uniapp+uview2对接Spring Cloud Alibaba AI实操步骤

### 🥦目标讲解

#### 一. Spring Cloud Alibaba AI 是什么

> **Spring Cloud Alibaba AI 是一个基于 Spring AI 的扩展框架**，它提供了对**阿里云通义系列大模型的支持**，使得开发者能够更容易地在 Java 应用程序中集成和使用这些 AI 模型。

##### 基础框架

Spring Cloud Alibaba AI **基于 Spring AI 构建**，这意味着它继承了 Spring AI 的核心功能，并在此基础上增加了特定于阿里云通义系列大模型的功能。

##### Spring AI

Spring AI 是一个用于简化机器学习和人工智能应用开发的框架，它提供了一套工具和抽象层来帮助开发者更容易地将 AI 功能集成到他们的应用程序中。

##### 通义系列大模型

阿里云的一系列预训练大模型，如**通义千问**等，它们可以用于多种场景，包括但不限于文本生成、对话系统、文档检索等。

##### 快速集成

Spring Cloud Alibaba AI 提供了简便的方式来接入这些大模型，使得开发者可以在较短的时间内完成 AI 功能的开发和部署。

##### Spring Boot 支持

为了使用 Spring Cloud Alibaba AI，你需要有一个基于 Spring Boot 的项目，并且 **Spring Boot 的版本需要是 3.x 或更高版本**。

##### JDK 版本要求

为了支持最新的特性和性能优化，**推荐使用 JDK 17 或更高版本**。

***

#### 二. 使用Spring Cloud Alibaba AI 前置条件

##### 1.JDK版本17+

> 如果有条件直接JDK21，一步到位，本狗使用的JDK21

[JDK21官网地址](https://www.oracle.com/java/technologies/downloads/#java21)

![](https://img.javadog.net/blog/alibaba-ai/0f2cbee4113b4510bc0d1ceebc3ca6e0.png)

##### 2.SringBoot3.0+

> 本狗使用SringBoot3.1.10版本

[SpringBoot文档官网地址](https://spring.io/projects/spring-boot#learn)

![](https://img.javadog.net/blog/alibaba-ai/a3ef0ebd1fe245c7a7bb711f9ebb83d3.png)

##### 3.申请通义API-KEY

> 申请调用Spring Cloud Alibaba AI的API-KEY

[DashScope模型服务灵积](https://dashscope.console.aliyun.com/overview)

![](https://img.javadog.net/blog/alibaba-ai/950d3109860a437083bf5c51698e4fbf.png)

![](https://img.javadog.net/blog/alibaba-ai/37532cc0c38c479bbf419b05a09ae35d.png)

***

#### 三. SpringBoot+uniapp+uview2对接Spring Cloud Alibaba AI实操步骤

![](https://img.javadog.net/blog/alibaba-ai/d75d719ac6e241d6b94f990e1d85c029.jpg)

#### 1.准备

##### 开发工具

| 工具         | 版本       | 用途             |
| ---------- | -------- | -------------- |
| IDEA       | 2023.3.3 | Java开发工具       |
| HBuilder X | 3.98     | 前端开发者服务的通用 IDE |

##### API-KEY申请

| 账号  | 功能      | 描述                |
| --- | ------- | ----------------- |
| 阿里云 | API-KEY | 前置中申请阿里AI的API-KEY |

##### 主要组件

*   前端

| 插件           | 版本      | 用途                     |
| ------------ | ------- | ---------------------- |
| uview-ui     | ^2.0.31 | 多平台快速开发的UI框架           |
| mescroll-uni | 1.3.7   | mescroll高性能的下拉刷新上拉加载组件 |

*   后端

| 插件                          | 版本         | 用途              |
| --------------------------- | ---------- | --------------- |
| jdk                         | 21         | java环境          |
| lombok                      | 1.18.30    | 代码简化插件          |
| maven                       | 3.6.3      | 包管理工具           |
| druid                       | 1.1.24     | JDBC组件          |
| hutool                      | 5.8.29     | Java工具类库        |
| mybatis-plus                | 3.5.7      | 基于 MyBatis 增强工具 |
| mysql                       | 8.0        | 数据库             |
| **spring-cloud-alibaba-ai** | 2023.0.1.0 | 阿里提供的 API       |

#### 2.功能分析

##### 后端

提供用户**登录，注册，聊天等接口，对接spring-cloud-alibaba-ai**。

##### 前端

画出**登录，注册，聊天页面，消息分页，下拉加载历史数据，AI消息打印展现效果**等页面操作。

##### 对接

前后端对接，实现**智能AI聊天**基本功能。

#### 3.开发

##### 后端代码总览

![](https://img.javadog.net/blog/alibaba-ai/c97acff617704263b2f573558a1f4cd8.png)

具体分层搭建此处省略，如有疑问请参考

[【SpringBoot】还不会SpringBoot项目模块分层？来这手把手教你](https://blog.csdn.net/baidu_25986059/article/details/128739849) 完全跟着复制分层即可。

**doc文件夹下有DB脚本，别忘跑！别忘跑！别忘跑！**

##### 后端-关键点

引入**spring-cloud-alibaba-ai**依赖，版本为 \<openai.version>2023.0.1.0\</openai.version>

```xml
 <!--阿里云为分布式应用开发提供了一站式解决方案-->
  <dependency>
       <groupId>com.alibaba.cloud</groupId>
       <artifactId>spring-cloud-alibaba-dependencies</artifactId>
       <version>${spring-cloud-alibaba.version}</version>
       <type>pom</type>
       <scope>import</scope>
   </dependency>

   <!--阿里云为分布式应用开发提供AI集成-->
   <dependency>
       <groupId>com.alibaba.cloud</groupId>
       <artifactId>spring-cloud-starter-alibaba-ai</artifactId>
       <version>${spring-cloud-alibaba-ai.version}</version>
   </dependency>
```

调用**alibaba-ai**方法

参考文档<https://sca.aliyun.com/docs/2023/user-guide/ai/quick-start/>

```java
    private String handleAI(Message message){
        Generation gen = new Generation();
        com.alibaba.dashscope.common.Message systemMsg = com.alibaba.dashscope.common.Message.builder()
                .role(Role.SYSTEM.getValue())
                .content("中文回复")
                .build();
        com.alibaba.dashscope.common.Message userMsg = com.alibaba.dashscope.common.Message.builder()
                .role(Role.USER.getValue())
                .content(message.getMsgContent())
                .build();
        GenerationParam param = GenerationParam.builder()
                .model("qwen-turbo")
                .messages(Arrays.asList(systemMsg, userMsg))
                .resultFormat(GenerationParam.ResultFormat.MESSAGE)
                .topK(50)
                .temperature(0.8f)
                .topP(0.8)
                .seed(1234)
                .build();
        try {
            return gen.call(param).getOutput().getChoices().get(0).getMessage().getContent();
        } catch (NoApiKeyException e) {
            throw new RuntimeException(e);
        } catch (InputRequiredException e) {
            throw new RuntimeException(e);
        }
    }
```

一定修application.yml配置文件中的api-key

      cloud:
        ai:
          tongyi:
            # 一定替换成自己的,也就是上方前置准备那里从阿里控制台获取的api-key
            api-key: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

##### 前端代码总览

![](https://img.javadog.net/blog/alibaba-ai/4758b1077f734448a5e6bbfd323d4a48.png)

##### 前端-关键点

**如何实现打字机效果？**

> 原理就是采用定时器，每次截取一个字节进行内容填充

```js
// interval
intervalFunc(){
   // 深拷贝内容
   let content = uni.$u.deepClone(this.targetContent);
   // 记录次数
   this.times++;
   if(this.times == content.length){
     clearInterval(this.interval)
   }
   this.targetMsg.msgContent = content.substring(0,this.times);
   this.$set(this.msgList, this.msgList.length-1, this.targetMsg);
   this.$nextTick(function () {
     this.mescroll.scrollTo(99999, 0)
   })
 },
```

例：AI返回的数据为【我真的好喜欢你！】
每次substring截取一个字符，然后进行填充渲染结果为
我
我真
我真的
我真的好
我真的好喜欢你
...

这样就可以实现简单的**打字机效果**，网上还有通过CSS形式或更加简便方法实现，都可参考

#### 4.运行

##### 后端

*   修改环境变量

![](https://img.javadog.net/blog/alibaba-ai/ee60c54fcf624c02b7c79980479484b8.png)

*   直接在IDEA运行启动

![](https://img.javadog.net/blog/alibaba-ai/8ab0ab6919b1453596a7ba123e932786.png)

##### 前端

*   在项目目录中执行 **npm i** ，下载依赖

![](https://img.javadog.net/blog/alibaba-ai/67a674f54a65404dbec16e28e675ca7f.png)

*   修改环境变量，在/common/config/env.js下

![](https://img.javadog.net/blog/alibaba-ai/bf2d2a5ddbe2448a92fc370a82f84979.png)

*   然后再HBuilder X工具中点击运行即可

![](https://img.javadog.net/blog/alibaba-ai/3949992543324d7eb24b543ed35bd0d9.png)

#### 5.成果展示

![](https://img.javadog.net/blog/alibaba-ai/1ebca7637a804b2cab32a30676caa010.png)

## 总结

本文主要介绍了如何使用 **Spring Cloud Alibaba AI** 来构建一个**基于 Spring Boot 和 uni-app 的聊天机器人应用**。

关键点：

*   了解Spring Cloud Alibaba AI

*   前置条件: 包括使用 JDK 17+、Spring Boot 3.0+ 以及获取通义 API-KEY。

*   实操步骤: 分别从前端和后端的角度进行了详细的说明，包括开发工具的选择、主要组件介绍、功能分析及开发过程中的关键代码示例。

最终，本狗展示了如何成功地实现了一个具备基本聊天功能的 AI 应用。

通过这个项目，小伙伴可以了解到如何利用现有的技术和框架快速搭建一个智能聊天应用，并且可以进一步探索更多高级功能和技术细节。

### 🍈猜你想问

#### 如何与狗哥联系进行探讨

##### 关注公众号【JavaDog程序狗】

公众号回复【入群】或者【加入】，便可成为【程序员学习交流摸鱼群】的一员，问题随便问，牛逼随便吹，目前**群内已有超过300+个小伙伴啦**！！！

![](https://img-blog.csdnimg.cn/img_convert/e98d36c93ee2d2e1d59adb7005ca5486.png)

##### 2.踩踩狗哥博客

[javadog.net](https://www.javadog.net/)

**里面有狗哥的私密联系方式呦 😘**

> 大家可以在里面留言，随意发挥，有问必答

![](https://img.javadog.net/blog/alibaba-ai/e0650478576648aeb5ca12d5f32199ee.png)

***

### 🍯猜你喜欢

#### 文章推荐

[【Java】@Transactional事务套着ReentrantLock锁，锁竟然失效超卖了](https://mp.weixin.qq.com/s/QkvbWRleIaNAH_Txo1aUZg)

[【规范】Git分支管理，看看我司是咋整的](https://mp.weixin.qq.com/s/8LRB9k-4EsgSN1lCy5az8A)

[【工具】珍藏免费宝藏工具，不好用你来捶我](https://mp.weixin.qq.com/s/Mj5--CQVaafs_bInMCgUaQ)

[【插件】IDEA这款插件，爱到无法自拔](https://mp.weixin.qq.com/s/IePixEWV5JMG1X2R4mwt6g)

[【规范】看看人家Git提交描述，那叫一个规矩](https://mp.weixin.qq.com/s/EbNWRpSYMdWFv5aUQ2ockw)

[【工具】用nvm管理nodejs版本切换，真香！](https://mp.weixin.qq.com/s/N6qwQpH-oIgFGSWIVDJ-2g)

[【项目实战】SpringBoot+uniapp+uview2打造H5+小程序+APP入门学习的聊天小项目](https://mp.weixin.qq.com/s/g7AZOWLgW5vcCahyJDEPKA)

[【项目实战】SpringBoot+uniapp+uview2打造一个企业黑红名单吐槽小程序](https://mp.weixin.qq.com/s/t_qwF_HvkdW-6TI3sYUHrA)

[【模块分层】还不会SpringBoot项目模块分层？来这手把手教你！](https://mp.weixin.qq.com/s/fpkiNR2tj832a6VxZozwDg)

![](https://img.javadog.net/blog/git-branch/a9b1d90b7430856edba3b9b22bda219c.jpg)
